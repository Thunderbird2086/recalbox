diff --git a/README.md b/README.md
index a4433b6..7e77eb1 100644
--- a/README.md
+++ b/README.md
@@ -12,6 +12,10 @@ The RaspberryPi is an amazing tool I discovered a month ago. The RetroPie projec
 
 So i started to wire my joysticks and buttons to my raspberry pi, and I wrote the first half of this driver in order to wire my joysticks and buttons directly to the RPi GPIOs.
 
+However, the Raspberry Pi Board B Rev 2 has a maximum of 21 usable GPIOs, not enough to wire all the 28 switches (2 joystick and 20 buttons) that a standard panel requires.
+
+UPDATE 0.1.5 : Added GPIO customization
+
 UPDATE 0.1.4 : Compatibily with rpi2 
 
 UPDATE 0.1.3 : Compatibily with 3.18.3 :
@@ -26,14 +30,22 @@ UPDATE 0.1.1 : RPi B+ VERSION :
 
 The new Raspberry Pi B+ Revision brought us 9 more GPIOs, so we are now able to connect 2 joysticks and 12 buttons directly to GPIOs. I updated the driver in order to support the 2 joysticks on GPIO configuration.
 
+### Even More Joysticks ###
+
+A little cheap chip named MCP23017 allows you to add 16 external GPIO, and take only two GPIO on the RPi. The chip allows you to use GPIO as output or input, input is what we are looking for if we want even more joysticks. 
+
+If you want to use more than one chip, the i2c protocol lets you choose different addresses for the connected peripheral, but all use the same SDA and SCL GPIOs.
+
+In theory you can connect up to 8 chips so 8 joystick.
+
 ## The Software ##
 The joystick driver is based on the gamecon_gpio_rpi driver by [marqs](https://github.com/marqs85)
 
-It is written for 4 directions joysticks and 9 buttons per player.
+It is written for 4 directions joysticks and 8 buttons per player. Using a MCP23017 extends input numbers to 16 : 4 directions and 12 buttons.
 
-It can read one joystick + 9 buttons wired on RPi GPIOs (two on RPi B+ revision)  
+It can read one joystick + buttons wired on RPi GPIOs (two on RPi B+ revision) and up to 5 other joysticks + buttons from MCP23017 chips. One MCP23017 is required for each joystick.
 
-It uses internal pull-ups of RPi, so all switches must be directly connected to its corresponding GPIO and to the ground.
+It uses internal pull-ups of RPi and of MCP23017, so all switches must be directly connected to its corresponding GPIO and to the ground.
 
 
 ## Common Case : Joysticks connected to GPIOs ##
@@ -46,7 +58,7 @@ Let's consider a 7 buttons cab panel with this button order :
     ← →	 Ⓑ Ⓐ Ⓡ Ⓗ
      ↓  
 
-With Ⓡ = Right trigger = TR and Ⓛ  = Left rtigger = TL  
+With Ⓡ = Right trigger = TR and Ⓛ  = Left trigger = TL  
 Ⓗ is the Hotkey.
 
 Here is the rev B GPIO pinout summary :
@@ -68,7 +80,7 @@ Download the installation script :
 ```shell
 mkdir mkjoystick
 cd mkjoystick
-wget https://github.com/digitalLumberjack/mk_arcade_joystick_rpi/releases/download/v0.1.4/install.sh
+wget https://github.com/recalbox/mk_arcade_joystick_rpi/releases/download/v0.1.4/install.sh
 ```
 
 Update your system :
@@ -101,13 +113,34 @@ sudo apt-get install -y --force-yes dkms cpp-4.7 gcc-4.7 git joystick
 
 2 - Install last kernel headers :
 ```shell
-wget http://www.niksula.hut.fi/~mhiienka/Rpi/linux-headers-rpi/linux-headers-`uname -r`_`uname -r`-2_armhf.deb
-sudo dpkg -i linux-headers-`uname -r`_`uname -r`-2_armhf.deb
-sudo rm linux-headers-`uname -r`_`uname -r`-2_armhf.deb
+sudo apt-get install -y --force-yes raspberrypi-kernel-headers
 ```
 
-3 - Compile the driver
-TODO
+3.a - Install driver from release (prefered):  
+```shell
+wget https://github.com/recalbox/mk_arcade_joystick_rpi/releases/download/v0.1.4/mk-arcade-joystick-rpi-0.1.4.deb
+sudo dpkg -i mk-arcade-joystick-rpi-0.1.4.deb
+```
+3.b - Or compile and install with dkms:  
+
+3.b.1 - Download the files:
+```shell
+git clone https://github.com/pinuct/mk_arcade_joystick_rpi/tree/customgpiohotkey
+```
+3.b.2 - Create a folder under  "/usr/src/*module*-*module-version*/"
+```shell
+mkdir /usr/src/mk_arcade_joystick_rpi-0.1.5/
+```
+3.b.3 - Copy the files into the folder:
+```shell
+cd mk_arcade_joystick_rpi/
+cp -a * /usr/src/mk_arcade_joystick_rpi-0.1.5/
+```
+3.b.4 - Compile and install the module:
+```shell
+dkms build -m mk_arcade_joystick_rpi -v 0.1.5
+dkms install -m mk_arcade_joystick_rpi -v 0.1.5
+```
 
 ### Loading the driver ###
 
@@ -127,21 +160,53 @@ If you have two joysticks connected on your RPi B+ version you will have to run
 ```shell
 sudo modprobe mk_arcade_joystick_rpi map=1,2
 ```
+If you have a TFT screen connected on your RPi B+ you can't use all the gpios. You can run the following command for using only the gpios not used by the tft screen (Be careful, not all tft screen use the same pins. GPIOs used with this map: 21,13,26,19,5,6,22,4,20,17,27,16,12):
+
+```shell
+sudo modprobe mk_arcade_joystick_rpi map=4
+```
+
+If you don't want to use all pins or wants a **custom gpio** map use:
+```shell
+sudo modprobe mk_arcade_joystick_rpi map=5 gpio=pin1,pin2,pin3,.....,pin12
+```
+Where *pinx* is the number of the gpio you want. There are 12 posible gpio with **button order: Y-,Y+,X-,X+,start,select,a,b,tr,y,x,tl,hk.** Use -1 for unused pins. For example `gpio=21,13,26,19,-1,-1,22,24,-1,-1,-1,-1,-1` uses gpios 21,13,26,19 for axis and gpios 22 and 24 for A and B buttons, the rest of buttons are unused.
+
+If you want customization for both players use `map=4,5` and `gpio=` for player 1 and `gpio2=` for player 2. Example:
+
+```
+# same configuration as map=1,2 but manually:
+sudo modprobe mk_arcade_joystick_rpi map=5,6 gpio=4,17,27,22,10,9,25,24,23,18,15,14,2 gpio2=11,5,6,13,19,26,21,20,16,12,7,8,3
+
+# If you use a TFT and want two players, you can´t use all gpios.
+# Example with two players and only axes, A and B buttons
+sudo modprobe mk_arcade_joystick_rpi map=5,6 gpio=4,17,27,22,-1,-1,25,24,-1,-1,-1,-1,-1 gpio2=11,5,6,13,19,-1,-1,20,16,-1,-1,-1,-1,-1
+```
 
 The GPIO joystick 1 events will be reported to the file "/dev/input/js0" and the GPIO joystick 2  events will be reported to "/dev/input/js1"
 
 ### Auto load at startup ###
 
-Open /etc/modules :
+Open `/etc/modules` :
 
 ```shell
 sudo nano /etc/modules
 ```
 
-And add the line you use to load the driver : 
+and add the line you use to load the driver : 
 
 ```shell
-mk_arcade_joystick_rpi map=1,2
+mk_arcade_joystick_rpi
+```
+
+Then create the file `/etc/modprobe.d/mk_arcade_joystick.conf` :
+```shell
+sudo nano /etc/modprobe.d/mk_arcade_joystick.conf
+```
+
+and add the module configuration : 
+```shell
+options mk_arcade_joystick_rpi map=1,2
 ```
 
 ### Testing ###
@@ -151,6 +216,94 @@ Use the following command to test joysticks inputs :
 jstest /dev/input/js0
 ```
 
+
+## More Joysticks case : MCP23017 ##
+
+
+Here is the MCP23017 pinout summary :
+
+
+![MCP23017 Interface](https://github.com/recalbox/mk_arcade_joystick_rpi/raw/hotkeybtn/wiki/images/mk_joystick_arcade_mcp23017.png)
+
+
+### Preparation of the RPi for MCP23017###
+
+Follow the standards installation instructions.
+
+Activate i2c on your RPi :
+```shell
+sudo nano /etc/modules
+```
+Add the following lines in order to load i2c modules automatically :
+```shell
+i2c-bcm2708 
+i2c-dev
+```
+
+And if the file /etc/modprobe.d/raspi-blacklist.conf exists : 
+```shell
+sudo nano /etc/modprobe.d/raspi-blacklist.conf
+```
+
+Check if you have a line with :
+```shell
+i2c-bcm2708 
+```
+and add a # at the beginning of the line to remove the blacklisting
+
+Reboot or load the two module :
+```shell
+modprobe i2c-bcm2708 i2c-dev
+```
+
+### Preparation of the MCP23017 chip ###
+
+
+You must set the pins A0 A1 and A2 to 0 or 1 in order to set the i2c address of the chip. If you only have 1 chip, connect the 3 pins to the ground.
+Just connect one of the pins to 3.3v to set its state to 1 and change the i2c address of the MCP23017.
+
+You must also connect the RESET pin to 3.3v.
+
+
+
+
+### Configuration ###
+When you want to load the driver you must pass a list of parameters that represent the list of connected Joysticks. The first parameter will be the joystick mapped to /dev/input/js0, the second to js1 etc..
+
+If you have connected a joystick on RPi GPIOs you must pass "1" as a parameter.
+
+If you have connected one or more joysticks with MCP23017, you must pass the address of I2C peripherals connected, which you can get with the command :
+
+```shell
+sudo i2cdetect -y 1
+```
+
+The configuration of each MCP23017 is done by setting pads A0 A1 and A2 to 0 or 1.
+
+If you configured your MCP23017 with A0 A1 and A2 connected to the ground, the address returned by i2cdetect should be 0x20
+
+So if you have a joystick connected to RPi GPIOs and a joystick on a MCP23017 with the address 0x20, in order to load the driver, you must run the command :
+
+```shell
+sudo modprobe mk_arcade_joystick_rpi map=1,0x20
+```
+
+The GPIO joystick events will be reported to the file "/dev/input/js0" and the mcp23017 joystick events will be reported to "/dev/input/js1"
+
+I tested up to 3 joystick, one on GPIOs, one on a MCP23017 on address 0x20, one on a MCP23017 on address 0x24 :
+
+```shell
+sudo modprobe mk_arcade_joystick_rpi map=1,0x20,0x24
+```
+
+
+## Known Bugs ##
+If you try to read or write on i2c with a tool like i2cget or i2cset when the driver is loaded, you are gonna have a bad time... 
+
+If you try i2cdetect when the driver is running, it will show you strange peripheral addresses...
+
+256MB Raspberry Pi Model B is not supported by the current driver. If you want to make the driver work on your old RPi, you will have to change the address of BSC1_BASE to (BCM2708_PERI_BASE + 0x205000) in order to use the correct i2c address, and recompile.
+
 Credits
 -------------
 -  [gamecon_gpio_rpi](https://github.com/petrockblog/RetroPie-Setup/wiki/gamecon_gpio_rpi) by [marqs](https://github.com/marqs85)
diff --git a/mk_arcade_joystick_rpi.c b/mk_arcade_joystick_rpi.c
index 76a81bc..d67ee76 100755
--- a/mk_arcade_joystick_rpi.c
+++ b/mk_arcade_joystick_rpi.c
@@ -42,7 +42,8 @@ MODULE_AUTHOR("Matthieu Proucelle");
 MODULE_DESCRIPTION("GPIO and MCP23017 Arcade Joystick Driver");
 MODULE_LICENSE("GPL");
 
-#define MK_MAX_DEVICES		2
+#define MK_MAX_DEVICES		9
+#define MK_MAX_BUTTONS      13
 
 #ifdef RPI2
 #define PERI_BASE        0x3F000000
@@ -64,7 +65,52 @@ MODULE_LICENSE("GPL");
 #define BSC1_BASE		(PERI_BASE + 0x804000)
 
 
+/*
+ * MCP23017 Defines
+ */
+#define MPC23017_GPIOA_MODE		0x00
+#define MPC23017_GPIOB_MODE		0x01
+#define MPC23017_GPIOA_PULLUPS_MODE	0x0c
+#define MPC23017_GPIOB_PULLUPS_MODE	0x0d
+#define MPC23017_GPIOA_READ             0x12
+#define MPC23017_GPIOB_READ             0x13
+
+/*
+ * Defines for I2C peripheral (aka BSC, or Broadcom Serial Controller)
+ */
+
+#define BSC1_C		*(bsc1 + 0x00)
+#define BSC1_S		*(bsc1 + 0x01)
+#define BSC1_DLEN	*(bsc1 + 0x02)
+#define BSC1_A		*(bsc1 + 0x03)
+#define BSC1_FIFO	*(bsc1 + 0x04)
+
+#define BSC_C_I2CEN	(1 << 15)
+#define BSC_C_INTR	(1 << 10)
+#define BSC_C_INTT	(1 << 9)
+#define BSC_C_INTD	(1 << 8)
+#define BSC_C_ST	(1 << 7)
+#define BSC_C_CLEAR	(1 << 4)
+#define BSC_C_READ	1
+
+#define START_READ	BSC_C_I2CEN|BSC_C_ST|BSC_C_CLEAR|BSC_C_READ
+#define START_WRITE	BSC_C_I2CEN|BSC_C_ST
+
+#define BSC_S_CLKT	(1 << 9)
+#define BSC_S_ERR	(1 << 8)
+#define BSC_S_RXF	(1 << 7)
+#define BSC_S_TXE	(1 << 6)
+#define BSC_S_RXD	(1 << 5)
+#define BSC_S_TXD	(1 << 4)
+#define BSC_S_RXR	(1 << 3)
+#define BSC_S_TXW	(1 << 2)
+#define BSC_S_DONE	(1 << 1)
+#define BSC_S_TA	1
+
+#define CLEAR_STATUS	BSC_S_CLKT|BSC_S_ERR|BSC_S_DONE
+
 static volatile unsigned *gpio;
+static volatile unsigned *bsc1;
 
 struct mk_config {
     int args[MK_MAX_DEVICES];
@@ -74,12 +120,33 @@ struct mk_config {
 static struct mk_config mk_cfg __initdata;
 
 module_param_array_named(map, mk_cfg.args, int, &(mk_cfg.nargs), 0);
-MODULE_PARM_DESC(map, "Enable or disable GPIO Arcade Joystick");
+MODULE_PARM_DESC(map, "Enable or disable GPIO, MCP23017, TFT and Custom Arcade Joystick");
+
+struct gpio_config {
+    int mk_arcade_gpio_maps_custom[MK_MAX_BUTTONS];
+    unsigned int nargs;
+};
+
+// for player 1 
+static struct gpio_config gpio_cfg __initdata;
+
+module_param_array_named(gpio, gpio_cfg.mk_arcade_gpio_maps_custom, int, &(gpio_cfg.nargs), 0);
+MODULE_PARM_DESC(gpio, "Numbers of custom GPIO for Arcade Joystick 1");
+
+// for player 2
+static struct gpio_config gpio_cfg2 __initdata;
+
+module_param_array_named(gpio2, gpio_cfg2.mk_arcade_gpio_maps_custom, int, &(gpio_cfg2.nargs), 0);
+MODULE_PARM_DESC(gpio2, "Numbers of custom GPIO for Arcade Joystick 2");
 
 enum mk_type {
     MK_NONE = 0,
     MK_ARCADE_GPIO,
     MK_ARCADE_GPIO_BPLUS,
+    MK_ARCADE_GPIO_TFT,
+    MK_ARCADE_GPIO_CUSTOM,
+    MK_ARCADE_GPIO_CUSTOM2,
+    MK_ARCADE_MCP23017,
     MK_MAX
 };
 
@@ -89,6 +156,8 @@ struct mk_pad {
     struct input_dev *dev;
     enum mk_type type;
     char phys[32];
+    int mcp23017addr;
+    int gpio_maps[MK_MAX_BUTTONS];
 };
 
 struct mk_nin_gpio {
@@ -105,9 +174,9 @@ struct mk_nin_gpio {
 struct mk {
     struct mk_pad pads[MK_MAX_DEVICES];
     struct timer_list timer;
+    int pad_count[MK_MAX];
     int used;
     struct mutex mutex;
-    int total_pads;
 };
 
 struct mk_subdev {
@@ -116,19 +185,29 @@ struct mk_subdev {
 
 static struct mk *mk_base;
 
-static const int mk_max_arcade_buttons = 13;
+static const int mk_data_size = 16;
+static const int mk_max_mcp_arcade_buttons = 16;
 
 // Map of the gpios :                     up, down, left, right, start, select, a,  b,  tr, y,  x,  tl, hk
 static const int mk_arcade_gpio_maps[] = {4,  17,    27,  22,    10,    9,      25, 24, 23, 18, 15, 14, 2 };
 // 2nd joystick on the b+ GPIOS                 up, down, left, right, start, select, a,  b,  tr, y,  x,  tl, hk
 static const int mk_arcade_gpio_maps_bplus[] = {11, 5,    6,    13,    19,    26,     21, 20, 16, 12, 7,  8,  3 };
 
+// Map joystick on the b+ GPIOS with TFT      up, down, left, right, start, select, a,  b,  tr, y,  x,  tl, hk
+static const int mk_arcade_gpio_maps_tft[] = {21, 13,    26,    19,    5,    6,     22, 4, 20, 17, 27,  16, 12};
+
+// Map of the mcp23017 on GPIOA            up, down, left, right, start, select, a, b
+static const int mk_arcade_gpioa_maps[] = {0,  1,    2,    3,     4,     5     , 6, 7};
+// Map of the mcp23017 on GPIOB            tr, y, x, tl, hk, tr2, z, tl2
+static const int mk_arcade_gpiob_maps[] = { 0, 1, 2,  3,  4,   5, 6,   7 };
+
+
 static const short mk_arcade_gpio_btn[] = {
-    BTN_START, BTN_SELECT, BTN_A, BTN_B, BTN_TR, BTN_Y, BTN_X, BTN_TL, BTN_MODE
+	BTN_START, BTN_SELECT, BTN_A, BTN_B, BTN_TR, BTN_Y, BTN_X, BTN_TL, BTN_MODE, BTN_TR2, BTN_Z, BTN_TL2
 };
 
 static const char *mk_names[] = {
-    NULL, "GPIO Controller 1", "GPIO Controller 2", "MCP23017 Controller"
+    NULL, "GPIO Controller 1", "GPIO Controller 2", "MCP23017 Controller", "GPIO Controller 1" , "GPIO Controller 1", "GPIO Controller 2"
 };
 
 /* GPIO UTILS */
@@ -145,22 +224,111 @@ static void setGpioAsInput(int gpioNum) {
     INP_GPIO(gpioNum);
 }
 
-static void mk_gpio_read_packet(struct mk_pad * pad, unsigned char *data) {
+static int getPullUpMask(int gpioMap[]){
+    int mask = 0x0000000;
     int i;
-    // Duplicate code to avoid the if at each loop
-    if (pad->type == MK_ARCADE_GPIO) {
-        for (i = 0; i < mk_max_arcade_buttons; i++) {
-            int read = GPIO_READ(mk_arcade_gpio_maps[i]);
-            if (read == 0) data[i] = 1;
-            else data[i] = 0;
+    for(i=0; i<MK_MAX_BUTTONS;i++) {
+        if(gpioMap[i] != -1){   // to avoid unused pins
+            int pin_mask  = 1<<gpioMap[i];
+            mask = mask | pin_mask;
+        }
+    }
+    return mask;
+}
+
+/* I2C UTILS */
+static void i2c_init(void) {
+    INP_GPIO(2);
+    SET_GPIO_ALT(2, 0);
+    INP_GPIO(3);
+    SET_GPIO_ALT(3, 0);
+}
+
+static void wait_i2c_done(void) {
+    while ((!((BSC1_S) & BSC_S_DONE))) {
+        udelay(100);
+    }
+}
+
+// Function to write data to an I2C device via the FIFO.  This doesn't refill the FIFO, so writes are limited to 16 bytes
+// including the register address. len specifies the number of bytes in the buffer.
+
+static void i2c_write(char dev_addr, char reg_addr, char *buf, unsigned short len) {
+
+    int idx;
+
+    BSC1_A = dev_addr;
+    BSC1_DLEN = len + 1; // one byte for the register address, plus the buffer length
+
+    BSC1_FIFO = reg_addr; // start register address
+    for (idx = 0; idx < len; idx++)
+        BSC1_FIFO = buf[idx];
+
+    BSC1_S = CLEAR_STATUS; // Reset status bits (see #define)
+    BSC1_C = START_WRITE; // Start Write (see #define)
+
+    wait_i2c_done();
+
+}
+
+// Function to read a number of bytes into a  buffer from the FIFO of the I2C controller
+
+static void i2c_read(char dev_addr, char reg_addr, char *buf, unsigned short len) {
+
+    unsigned short bufidx = 0;
+
+    i2c_write(dev_addr, reg_addr, NULL, 0);
+
+    memset(buf, 0, len); // clear the buffer
+
+    BSC1_DLEN = len;
+    BSC1_S = CLEAR_STATUS; // Reset status bits (see #define)
+    BSC1_C = START_READ; // Start Read after clearing FIFO (see #define)
+
+    do {
+        // Wait for some data to appear in the FIFO
+        while ((BSC1_S & BSC_S_TA) && !(BSC1_S & BSC_S_RXD));
+
+        // Consume the FIFO
+        while ((BSC1_S & BSC_S_RXD) && (bufidx < len)) {
+            buf[bufidx++] = BSC1_FIFO;
         }
-    }else if (pad->type == MK_ARCADE_GPIO_BPLUS) {
-         for (i = 0; i < mk_max_arcade_buttons; i++) {
-            int read = GPIO_READ(mk_arcade_gpio_maps_bplus[i]);
+    } while ((!(BSC1_S & BSC_S_DONE)));
+}
+
+/*  ------------------------------------------------------------------------------- */
+
+static void mk_mcp23017_read_packet(struct mk_pad * pad, unsigned char *data) {
+    int i;
+    char resultA, resultB;
+    i2c_read(pad->mcp23017addr, MPC23017_GPIOA_READ, &resultA, 1);
+    i2c_read(pad->mcp23017addr, MPC23017_GPIOB_READ, &resultB, 1);
+
+    // read direction
+    for (i = 0; i < 4; i++) {
+        data[i] = !((resultA >> mk_arcade_gpioa_maps[i]) & 0x1);
+    }
+    // read buttons on gpioa
+    for (i = 4; i < 8; i++) {
+        data[i] = !((resultA >> mk_arcade_gpioa_maps[i]) & 0x1);
+    }
+    // read buttons on gpiob
+    for (i = 8; i < 16; i++) {
+        data[i] = !((resultB >> (mk_arcade_gpiob_maps[i-8])) & 0x1);
+    }
+}
+
+static void mk_gpio_read_packet(struct mk_pad * pad, unsigned char *data) {
+    int i;
+
+    for (i = 0; i < MK_MAX_BUTTONS; i++) {
+        if(pad->gpio_maps[i] != -1){    // to avoid unused buttons
+            int read = GPIO_READ(pad->gpio_maps[i]);
             if (read == 0) data[i] = 1;
             else data[i] = 0;
-        }
+        }else data[i] = 0;
     }
+
 }
 
 static void mk_input_report(struct mk_pad * pad, unsigned char * data) {
@@ -168,22 +336,35 @@ static void mk_input_report(struct mk_pad * pad, unsigned char * data) {
     int j;
     input_report_abs(dev, ABS_Y, !data[0]-!data[1]);
     input_report_abs(dev, ABS_X, !data[2]-!data[3]);
-    for (j = 4; j < mk_max_arcade_buttons; j++) {
-        input_report_key(dev, mk_arcade_gpio_btn[j - 4], data[j]);
-    }
+	if (pad->type == MK_ARCADE_MCP23017) {	// check if MCP23017 and extend with 4.
+		for (j = 4; j < (mk_max_mcp_arcade_buttons); j++) {
+			input_report_key(dev, mk_arcade_gpio_btn[j - 4], data[j]);
+		}
+	}
+	else {
+		for (j = 4; j < MK_MAX_BUTTONS; j++) {
+			input_report_key(dev, mk_arcade_gpio_btn[j - 4], data[j]);
+		}
+	}
     input_sync(dev);
 }
 
 static void mk_process_packet(struct mk *mk) {
 
-    unsigned char data[mk_max_arcade_buttons];
+    unsigned char data[mk_data_size];
     struct mk_pad *pad;
     int i;
 
-    for (i = 0; i < mk->total_pads; i++) {
+    for (i = 0; i < MK_MAX_DEVICES; i++) {
         pad = &mk->pads[i];
-        mk_gpio_read_packet(pad, data);
-        mk_input_report(pad, data);
+        if (pad->type == MK_ARCADE_GPIO || pad->type == MK_ARCADE_GPIO_BPLUS || pad->type == MK_ARCADE_GPIO_TFT || pad->type == MK_ARCADE_GPIO_CUSTOM || pad->type == MK_ARCADE_GPIO_CUSTOM2) {
+            mk_gpio_read_packet(pad, data);
+            mk_input_report(pad, data);
+        }
+        if (pad->type == MK_ARCADE_MCP23017) {
+            mk_mcp23017_read_packet(pad, data);
+            mk_input_report(pad, data);
+        }
     }
 
 }
@@ -228,13 +409,46 @@ static int __init mk_setup_pad(struct mk *mk, int idx, int pad_type_arg) {
     struct input_dev *input_dev;
     int i, pad_type;
     int err;
+    char FF = 0xFF;
     pr_err("pad type : %d\n",pad_type_arg);
-    pad_type = pad_type_arg;
-  
-    if (pad_type < 1) {
+
+    if (pad_type_arg >= MK_MAX) {
+        pad_type = MK_ARCADE_MCP23017;
+    } else {
+        pad_type = pad_type_arg;
+    }
+
+    if (pad_type < 1 || pad_type >= MK_MAX) {
         pr_err("Pad type %d unknown\n", pad_type);
         return -EINVAL;
     }
+
+    if (pad_type == MK_ARCADE_GPIO_CUSTOM) {
+
+        // if the device is custom, be sure to get correct pins
+        if (gpio_cfg.nargs < 1) {
+            pr_err("Custom device needs gpio argument\n");
+            return -EINVAL;
+        } else if(gpio_cfg.nargs != MK_MAX_BUTTONS){
+             pr_err("Invalid gpio argument %d\n", pad_type);
+             return -EINVAL;
+        }
+    
+    }
+
+    if (pad_type == MK_ARCADE_GPIO_CUSTOM2) {
+
+        // if the device is custom, be sure to get correct pins
+        if (gpio_cfg2.nargs < 1) {
+            pr_err("Custom device needs gpio argument\n");
+            return -EINVAL;
+        } else if(gpio_cfg2.nargs != MK_MAX_BUTTONS){
+             pr_err("Invalid gpio argument %d\n", pad_type);
+             return -EINVAL;
+        }
+    
+    }
+
     pr_err("pad type : %d\n",pad_type);
     pad->dev = input_dev = input_allocate_device();
     if (!input_dev) {
@@ -243,6 +457,7 @@ static int __init mk_setup_pad(struct mk *mk, int idx, int pad_type_arg) {
     }
 
     pad->type = pad_type;
+    pad->mcp23017addr = pad_type_arg;
     snprintf(pad->phys, sizeof (pad->phys),
             "input%d", idx);
 
@@ -262,28 +477,71 @@ static int __init mk_setup_pad(struct mk *mk, int idx, int pad_type_arg) {
 
     for (i = 0; i < 2; i++)
         input_set_abs_params(input_dev, ABS_X + i, -1, 1, 0, 0);
-    for (i = 0; i < mk_max_arcade_buttons - 4; i++)
-        __set_bit(mk_arcade_gpio_btn[i], input_dev->keybit);
-
-    mk->total_pads++;
-
+	if (pad_type != MK_ARCADE_MCP23017)
+	{
+		for (i = 0; i < MK_MAX_BUTTONS - 4; i++)
+			__set_bit(mk_arcade_gpio_btn[i], input_dev->keybit);
+	}
+	else { //Checking for MCP23017 so it gets 4 more buttons registered to it.
+		for (i = 0; i < mk_max_mcp_arcade_buttons - 4; i++)
+			__set_bit(mk_arcade_gpio_btn[i], input_dev->keybit);
+	}
+
+    mk->pad_count[pad_type]++;
+
+    // asign gpio pins
     switch (pad_type) {
         case MK_ARCADE_GPIO:
-            for (i = 0; i < mk_max_arcade_buttons; i++) {
-                setGpioAsInput(mk_arcade_gpio_maps[i]);
-            }
-            setGpioPullUps(0xBC6C61C);
-            printk("GPIO configured for pad%d\n", idx);
+            memcpy(pad->gpio_maps, mk_arcade_gpio_maps, MK_MAX_BUTTONS *sizeof(int));
             break;
         case MK_ARCADE_GPIO_BPLUS:
-            for (i = 0; i < mk_max_arcade_buttons; i++) {
-                setGpioAsInput(mk_arcade_gpio_maps_bplus[i]);
-            }
-            setGpioPullUps(0xFFFFFFC);
-            printk("GPIO configured for pad%d\n", idx);
+            memcpy(pad->gpio_maps, mk_arcade_gpio_maps_bplus, MK_MAX_BUTTONS *sizeof(int));
+            break;
+        case MK_ARCADE_GPIO_TFT:
+            memcpy(pad->gpio_maps, mk_arcade_gpio_maps_tft, MK_MAX_BUTTONS *sizeof(int));
+            break;
+        case MK_ARCADE_GPIO_CUSTOM:
+            memcpy(pad->gpio_maps, gpio_cfg.mk_arcade_gpio_maps_custom, MK_MAX_BUTTONS *sizeof(int));
+            break;
+        case MK_ARCADE_GPIO_CUSTOM2:
+            memcpy(pad->gpio_maps, gpio_cfg2.mk_arcade_gpio_maps_custom, MK_MAX_BUTTONS *sizeof(int));
+            break;
+        case MK_ARCADE_MCP23017:
+            // nothing to asign if MCP23017 is used
             break;
     }
 
+    // initialize gpio if not MCP23017, else initialize i2c
+    if(pad_type != MK_ARCADE_MCP23017){
+        for (i = 0; i < MK_MAX_BUTTONS; i++) {
+            printk("GPIO = %d\n", pad->gpio_maps[i]);
+            if(pad->gpio_maps[i] != -1){    // to avoid unused buttons
+                 setGpioAsInput(pad->gpio_maps[i]);
+            }                
+        }
+        setGpioPullUps(getPullUpMask(pad->gpio_maps));
+        printk("GPIO configured for pad%d\n", idx);
+    }else{
+        i2c_init();
+        udelay(1000);
+        // Put all GPIOA inputs on MCP23017 in INPUT mode
+        i2c_write(pad->mcp23017addr, MPC23017_GPIOA_MODE, &FF, 1);
+        udelay(1000);
+        // Put all inputs on MCP23017 in pullup mode
+        i2c_write(pad->mcp23017addr, MPC23017_GPIOA_PULLUPS_MODE, &FF, 1);
+        udelay(1000);
+        // Put all GPIOB inputs on MCP23017 in INPUT mode
+        i2c_write(pad->mcp23017addr, MPC23017_GPIOB_MODE, &FF, 1);
+        udelay(1000);
+        // Put all inputs on MCP23017 in pullup mode
+        i2c_write(pad->mcp23017addr, MPC23017_GPIOB_PULLUPS_MODE, &FF, 1);
+        udelay(1000);
+        // Put all inputs on MCP23017 in pullup mode a second time
+        // Known bug : if you remove this line, you will not have pullups on GPIOB 
+        i2c_write(pad->mcp23017addr, MPC23017_GPIOB_PULLUPS_MODE, &FF, 1);
+        udelay(1000);
+    }
+
     err = input_register_device(pad->dev);
     if (err)
         goto err_free_dev;
@@ -356,6 +614,11 @@ static int __init mk_init(void) {
         pr_err("io remap failed\n");
         return -EBUSY;
     }
+    /* Set up i2c pointer for direct register access */
+    if ((bsc1 = ioremap(BSC1_BASE, 0xB0)) == NULL) {
+        pr_err("io remap failed\n");
+        return -EBUSY;
+    }
     if (mk_cfg.nargs < 1) {
         pr_err("at least one device must be specified\n");
         return -EINVAL;
@@ -372,6 +635,7 @@ static void __exit mk_exit(void) {
         mk_remove(mk_base);
 
     iounmap(gpio);
+    iounmap(bsc1);
 }
 
 module_init(mk_init);
diff --git a/utils/install.sh b/utils/install.sh
index 76827ee..e32879c 100755
--- a/utils/install.sh
+++ b/utils/install.sh
@@ -14,15 +14,11 @@ else
 	echo "Installing required dependencies"
 	sudo apt-get install -y --force-yes dkms cpp-4.7 gcc-4.7 git joystick || 
          { echo "ERROR : Unable to install required dependencies" && exit 1 ;}
-	echo "Downloading current kernel headers"
-	wget http://www.niksula.hut.fi/~mhiienka/Rpi/linux-headers-rpi/linux-headers-`uname -r`_`uname -r`-2_armhf.deb || 
-         { echo "ERROR : Unable to find kernel headers" && exit 1 ;}
-	echo "Installing current kernel headers"
-	sudo dpkg -i linux-headers-`uname -r`_`uname -r`-2_armhf.deb || 
+	echo "Downloading and installing current kernel headers"
+	sudo apt-get install -y --force-yes raspberrypi-kernel-headers || 
          { echo "ERROR : Unable to install kernel headers" && exit 1 ;}
-	rm linux-headers-`uname -r`_`uname -r`-2_armhf.deb
 	echo "Downloading mk_arcade_joystick_rpi 0.1.4"
-	wget https://github.com/digitalLumberjack/mk_arcade_joystick_rpi/releases/download/v0.1.4/mk-arcade-joystick-rpi-0.1.4.deb || 
+	wget https://github.com/recalbox/mk_arcade_joystick_rpi/releases/download/v0.1.4/mk-arcade-joystick-rpi-0.1.4.deb || 
          { echo "ERROR : Unable to find mk_arcade_joystick_package" && exit 1 ;}
 	echo "Installing mk_arcade_joystick_rpi 0.1.4"
 	sudo dpkg -i mk-arcade-joystick-rpi-0.1.4.deb || 
@@ -30,6 +26,5 @@ else
 	echo "Installation OK"
 	echo "Load the module with 'sudo modprobe mk_arcade_joystick_rpi map=1' for 1 joystick"
 	echo "or with 'sudo modprobe mk_arcade_joystick_rpi map=1,2' for 2 joysticks"
-	echo "See https://github.com/digitalLumberjack/mk_arcade_joystick_rpi#loading-the-driver for more details"
-fi
-
+	echo "See https://github.com/recalbox/mk_arcade_joystick_rpi#loading-the-driver for more details"
+fi
\ No newline at end of file
diff --git a/wiki/images/mk_joystick_arcade_mcp23017.png b/wiki/images/mk_joystick_arcade_mcp23017.png
index e7d7604..5b7cdbe 100644
Binary files a/wiki/images/mk_joystick_arcade_mcp23017.png and b/wiki/images/mk_joystick_arcade_mcp23017.png differ
diff --git a/wiki/images/mk_joystick_arcade_mcp23017.xcf b/wiki/images/mk_joystick_arcade_mcp23017.xcf
index d409161..fa456df 100644
Binary files a/wiki/images/mk_joystick_arcade_mcp23017.xcf and b/wiki/images/mk_joystick_arcade_mcp23017.xcf differ
